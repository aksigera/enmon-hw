import { MikroOrmModule } from "@mikro-orm/nestjs";
import { TsMorphMetadataProvider } from "@mikro-orm/reflection";
import { Module } from "@nestjs/common";
import { MongoMemoryServer } from 'mongodb-memory-server';

@Module({
    imports: [dbConfigFactory()],
})
export class DbModule {

}

async function dbConfigFactory() {
    return MikroOrmModule.forRoot({
        autoLoadEntities: true,
        metadataProvider: TsMorphMetadataProvider,
        ensureIndexes: true,
        type: 'mongo',
        dbName: 'enmon',
        clientUrl: process.env.NODE_ENV === 'test'
            ? (await MongoMemoryServer.create()).getUri()
            : (process.env.MONGO_URI ? process.env.MONGO_URI : "mongodb://root:example@localhost:27017/"),
    })
}
