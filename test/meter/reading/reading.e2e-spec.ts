import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { MeterProvider } from "test/meter/meter.provider";
import { ObjectId } from "@mikro-orm/mongodb";
import { testModule } from "test/util/init.util";
import * as _ from "lodash";

jest.setTimeout(30000)
/** These should be merged into single suite.
 * The reason it's not is that currently in-memory DB is preserved between tests. */
describe('E2E Reading CRUD', () => {
    let app: INestApplication;

    it('Happy path for multiple readings', async () => {
        const meter = await MeterProvider.createMeter(app)

        const readings = await Promise.all([
            MeterProvider.postReading(app, {meter: new ObjectId(meter.id)}),
            MeterProvider.postReading(app, {meter: new ObjectId(meter.id)}),
            MeterProvider.postReading(app),
        ])

        let resAll = await request(app.getHttpServer())
            .get('/meter/reading');

        expect(resAll.status).toEqual(200);
        expect(resAll.body).toEqual([readings, readings.length]);

        const resMeter = await request(app.getHttpServer())
            .get('/meter/' + meter.id);

        expect(resMeter.body.readings).toEqual(readings.slice(0, 2));

        const patch = {
            id: readings[2].id,
            counter: readings[2].counter + 200,
        }

        await request(app.getHttpServer())
            .patch('/meter/reading')
            .send(patch)
            .expect(200);

        await request(app.getHttpServer())
            .get('/meter/reading/' + patch.id)
            .expect({...readings[2], ...patch});

        await request(app.getHttpServer())
            .delete('/meter/reading/' + readings[2].id)
            .expect(200);

        resAll = await request(app.getHttpServer())
            .get('/meter/reading');

        expect(resAll.body).toEqual([_.dropRight(readings), 2])
    });

    beforeEach(async () => {
        app = (await testModule()).createNestApplication();
        await app.init();
    });
})
