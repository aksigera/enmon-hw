import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";
import { INestApplication, ValidationPipe } from "@nestjs/common";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    app.useGlobalGuards(app.get(JwtAuthGuard));
    app.useGlobalPipes(new ValidationPipe({transform: true}));

    swagger(app)

    await app.listen(3000);
}

bootstrap();

function swagger(app: INestApplication) {
    const config = new DocumentBuilder()
        .setTitle('Enmon HW')
        .setDescription('Enmon HW API')
        .setVersion('1.0')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('swagger', app, document);
}
