import { Injectable } from '@nestjs/common';
import { ReadingDto } from './reading.dto';
import { InjectRepository } from "@mikro-orm/nestjs";
import { Reading } from "./reading.entity";
import { EntityRepository, QueryFlag } from "@mikro-orm/core";
import { ObjectId } from "@mikro-orm/mongodb";
import { Meter } from "src/domain/meter/meter.entity";
import { ReadingPagingQuery } from "src/domain/meter/reading/reading.paging-query";

@Injectable()
export class ReadingService {
    constructor(
        @InjectRepository(Reading)
        private readonly repository: EntityRepository<Reading>,
        @InjectRepository(Meter)
        private readonly meterRepository: EntityRepository<Meter>,
    ) {
    }

    async create(dto: ReadingDto) {
        if (await this.meterRepository.findOne(dto.meter) === null) {
            console.warn(`Meter ${dto.meter} was not found!`)
        }

        const entity = this.repository.create({...dto, timestamp: new Date(dto.timestamp)})
        await this.repository.persistAndFlush(entity)
        return entity
    }

    findOrFail(id: ObjectId) {
        return this.repository.findOneOrFail(id)
    }

    findPaginated(query: ReadingPagingQuery) {
        return this.repository.findAndCount(
            {},
            {
                orderBy: {[query.field]: query.order},
                offset: query.offset,
                limit: query.limit,
                flags: [QueryFlag.PAGINATE],
            }
        );
    }

    async update(dto: ReadingDto) {
        const entity = await this.repository.findOneOrFail(new ObjectId(dto.id))

        return this.repository.nativeUpdate(entity, {
            ...dto,
            timestamp: dto.timestamp ? new Date(dto.timestamp) : entity.timestamp
        })
    }

    async remove(id: ObjectId) {
        const entity = await this.repository.findOneOrFail(id)

        await this.repository.removeAndFlush(entity)
    }
}
