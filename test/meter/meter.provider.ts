import { INestApplication } from "@nestjs/common";
import * as request from "supertest";
import { MeterDto } from "src/domain/meter/meter.dto";
import { ReadingDto } from "src/domain/meter/reading/reading.dto";
import { ObjectId } from "@mikro-orm/mongodb";

export class MeterProvider {
    static async createMeter(app: INestApplication) {
        const response = await request(app.getHttpServer())
            .post('/meter')
            .send({});

        expect(response.status).toEqual(201);

        return response.body as MeterDto
    }

    static async postReading(
        app: INestApplication,
        dto: Partial<ReadingDto> = {},
    ) {
        const response = await request(app.getHttpServer())
            .post('/meter/reading')
            .send(readingDefaults(dto));

        expect(response.status).toEqual(201);

        return response.body as ReadingDto
    }
}

function readingDefaults(dto: Partial<ReadingDto>): ReadingDto {
    return {
        timestamp: "2010-10-10T10:10:10.100",
        counter: 666.555,
        meter: new ObjectId("000000000000000000000000"),
        ...dto
    }
}
