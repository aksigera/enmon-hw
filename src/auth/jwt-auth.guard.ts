import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from "@nestjs/core";
import { AUTH_PUBLIC } from "src/auth/auth-public";

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
    constructor(
        private readonly reflector: Reflector,
    ) {
        super();
    }

    canActivate(context: ExecutionContext) {
        const publicEndpoint = this.reflector.getAllAndOverride<boolean>(AUTH_PUBLIC, [
            context.getHandler(),
            context.getClass(),
        ]);

        return publicEndpoint || super.canActivate(context);
    }
}
