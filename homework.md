# Enmon Technologies backend homework
Create a simple dockerized Node.js service with MongoDB as datastore. This service will provide two parts.
First part of the API is public and serves for login. With valid credentials it returns JWT access token.
The second part is protected and is accessible only with valid JWT token. This is simple CRUD for meter readings. 

## Guidelines
- TypeScript is optional
- Try to organize all files in appropriate directory hierarchy
- You can use any library of your choice

## Acceptance Criteria
- User is able to obtain JWT token by sending correct credentials. Credentials can be hardcoded.
- User is able to create new reading, get list of saved readings, update a reading and delete a reading.
- Backend is validating the payload provided by user and returns meaningful error messages (timestamp must be a valid date etc.)

## Stretch Goals
- Write unit tests
- Readings GET endpoint supports pagination

## Example reading model
```
    {
        "timestamp": "2021-10-01T15:30:30.568", // ISODate
        "counter": 5646.5, // Float
        "meter": "507f191e810c19729de860ea", // ObjectId
    };
```
