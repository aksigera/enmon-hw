import { ObjectId } from "@mikro-orm/mongodb";
import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsMongoId, IsPositive } from "class-validator";

export class ReadingDto {
    @ApiProperty()
    id?: string

    /** Here I consider that timestamp is sent by the meter and can differ from Date.now()
     * (e.g. because of network problems) */
    @ApiProperty()
    @IsDateString()
    timestamp: string

    @ApiProperty()
    @IsPositive()
    counter: number

    @ApiProperty()
    @IsMongoId()
    meter: ObjectId

    constructor(timestamp: string, counter: number, meter: ObjectId) {
        this.timestamp = timestamp
        this.counter = counter
        this.meter = meter
    }
}
