import { Module } from '@nestjs/common';
import { MeterModule } from "src/domain/meter/meter.module";
import { AuthModule } from "src/auth/auth.module";
import { DbModule } from "src/db/db.module";
import { AuthController } from "src/auth/auth.controller";

@Module({
    imports: [
        MeterModule,
        AuthModule,
        DbModule,
    ],
    controllers: [AuthController],
})
export class AppModule {

}
