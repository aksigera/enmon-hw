import { Entity, ManyToOne, Property } from "@mikro-orm/core";
import { Meter } from "src/domain/meter/meter.entity";
import { BaseEntity } from "src/domain/common/base.entity";

@Entity()
export class Reading extends BaseEntity {

    @ManyToOne({serializer: value => value.id})
    meter!: Meter;

    @Property()
    timestamp!: Date

    @Property()
    counter!: number
}
