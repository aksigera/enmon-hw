export type OrderableKeys<T extends object> = {
    [K in keyof T]: T[K] extends string | Date | number ? K : never
}[keyof T];
