import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { MeterProvider } from "test/meter/meter.provider";
import { testModule } from "test/util/init.util";

describe('E2E Meter CRUD', () => {
    let app: INestApplication;

    it('Create and read', async () => {
        const meter = await MeterProvider.createMeter(app)

        const getRes = await request(app.getHttpServer())
            .get('/meter/' + meter.id);

        expect(getRes.status).toEqual(200);
        expect(getRes.body).toEqual(meter);
    });

    beforeEach(async () => {
        app = (await testModule()).createNestApplication();
        app.useGlobalPipes(new ValidationPipe({transform: true}));
        await app.init();
    });
})
