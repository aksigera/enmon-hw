import { Body, Controller, Get, Param, Post } from '@nestjs/common'
import { MeterService } from './meter.service'
import { MeterDto } from './meter.dto'
import { ObjectId } from "@mikro-orm/mongodb"

@Controller('meter')
export class MeterController {
    constructor(
        private readonly meterService: MeterService,
    ) {
    }

    @Post()
    create(@Body() meter: MeterDto) {
        return this.meterService.create(meter)
    }

    @Get()
    findAll() {
        return this.meterService.findAll()
    }

    @Get(":id")
    findOne(@Param() id: string) {
        return this.meterService.findOne(new ObjectId(id))
    }
}
