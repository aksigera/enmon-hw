import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common'
import { ReadingService } from './reading.service'
import { ReadingDto } from './reading.dto'
import { ObjectId } from "@mikro-orm/mongodb"
import { ReadingPagingQuery } from "src/domain/meter/reading/reading.paging-query";

@Controller('meter/reading')
export class ReadingController {
    constructor(
        private readonly readingService: ReadingService,
    ) {
    }

    @Post()
    create(@Body() reading: ReadingDto) {
        return this.readingService.create(reading)
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.readingService.findOrFail(new ObjectId(id))
    }

    @Get()
    findAll(@Query() query: ReadingPagingQuery) {
        return this.readingService.findPaginated(query)
    }

    @Patch()
    update(@Body() reading: ReadingDto) {
        return this.readingService.update(reading)
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.readingService.remove(new ObjectId(id))
    }
}
