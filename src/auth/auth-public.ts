import { SetMetadata } from "@nestjs/common";

export const AUTH_PUBLIC = 'isPublic';
export const AuthPublic = () => SetMetadata(AUTH_PUBLIC, true);
