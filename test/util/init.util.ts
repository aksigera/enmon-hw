import { Test } from "@nestjs/testing";
import { AppModule } from "src/app.module";

export async function testModule() {
    return await Test.createTestingModule({
        imports: [AppModule],
    }).compile()
}
