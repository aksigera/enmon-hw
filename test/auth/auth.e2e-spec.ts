import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";
import { testModule } from "test/util/init.util";

describe('Auth', () => {
    let app: INestApplication;

    it('Returns 401 when calling secured endpoint w/o access_token.', async () => {
        await request(app.getHttpServer())
            .get('/meter/reading')
            .expect(401);
    });

    it('Returns 401 for wrong password.', async () => {
        await request(app.getHttpServer())
            .post('/auth/login')
            .send({
                "username": "john",
                "password": "wrong"
            })
            .expect(401);
    });

    it('Returns valid JWT on successful login.', async () => {
        const response = await request(app.getHttpServer())
            .post('/auth/login')
            .send({
                "username": "john",
                "password": "changed"
            });

        expect(response.status).toEqual(201);

        await request(app.getHttpServer())
            .get('/meter/reading')
            .auth(response.body.access_token, {type: "bearer"})
            .expect(200)
    });

    it('Health check', () => {
        return request(app.getHttpServer())
            .get('/health')
            .expect(200)
            .expect('Healthy');
    });

    beforeEach(async () => {
        app = (await testModule()).createNestApplication();
        app.useGlobalGuards(app.get(JwtAuthGuard));

        await app.init();
    });
})
