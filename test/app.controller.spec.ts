import { AuthController } from "src/auth/auth.controller";
import { testModule } from "test/util/init.util";

describe('AuthController', () => {
    let controller: AuthController;

    beforeEach(async () => {
        controller = (await testModule()).get(AuthController);
    });

    describe('Health check', () => {
        it('should return "Healthy"', () => {
            expect(controller.healthCheck()).toBe('Healthy');
        });
    });
});
