import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsInt, IsOptional, IsPositive, Min } from "class-validator";
import { QueryOrder } from "@mikro-orm/core";
import { Type } from "class-transformer";

/**
 * FYI: Query object props are not type-coerced automatically, so we need to do so explicitly using @Type(() => Number)
 */
export abstract class BasePagingQuery {
    @ApiProperty()
    @IsOptional()
    @IsInt()
    @Min(0)
    @Type(() => Number)
    offset: number = 0

    @ApiProperty()
    @IsOptional()
    @IsInt()
    @IsPositive()
    @Type(() => Number)
    limit: number | undefined

    @ApiProperty()
    @IsOptional()
    @IsEnum(QueryOrder)
    order: QueryOrder = QueryOrder.ASC
}
