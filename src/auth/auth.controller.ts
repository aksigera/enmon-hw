import { Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { LocalAuthGuard } from "src/auth/local-auth.guard";
import { AuthService } from "src/auth/auth.service";
import { AuthPublic } from "src/auth/auth-public";

@Controller()
@AuthPublic()
export class AuthController {
    constructor(
        private readonly authService: AuthService,
    ) {
    }

    @Get("health")
    healthCheck(): string {
        return "Healthy";
    }

    @UseGuards(LocalAuthGuard)
    @Post('auth/login')
    async login(@Request() req) {
        return this.authService.login(req.user);
    }
}
