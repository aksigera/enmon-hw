import { Module } from '@nestjs/common';
import { MeterService } from './meter.service';
import { MeterController } from './meter.controller';
import { MikroOrmModule } from "@mikro-orm/nestjs";
import { Meter } from "./meter.entity";
import { Reading } from "src/domain/meter/reading/reading.entity";
import { ReadingController } from "src/domain/meter/reading/reading.controller";
import { ReadingService } from "src/domain/meter/reading/reading.service";

@Module({
    imports: [MikroOrmModule.forFeature([Meter, Reading])],
    controllers: [ReadingController, MeterController],
    providers: [MeterService, ReadingService],
})
export class MeterModule {
}
