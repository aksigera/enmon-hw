import { Injectable } from '@nestjs/common';
import { MeterDto } from './meter.dto';
import { InjectRepository } from "@mikro-orm/nestjs";
import { Meter } from "./meter.entity";
import { EntityRepository } from "@mikro-orm/core";
import { ObjectId } from "@mikro-orm/mongodb";

@Injectable()
export class MeterService {
    constructor(
        @InjectRepository(Meter)
        private readonly repository: EntityRepository<Meter>,
    ) {
    }

    async create(dto: MeterDto) {
        const entity = this.repository.create(dto)
        await this.repository.persistAndFlush(entity)
        return entity
    }

    findOne(id: ObjectId) {
        return this.repository.findOne(id, {populate: ['readings']})
    }

    findAll() {
        return this.repository.findAll()
    }

}
