import { ApiProperty } from '@nestjs/swagger';

export class MeterDto {
    constructor(id: string) {
        this.id = id
    }

    @ApiProperty()
    id: string
}
