import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserModule } from "src/domain/user/user.module";
import { PassportModule } from "@nestjs/passport";
import { LocalStrategy } from "src/auth/local.strategy";
import { JwtModule } from "@nestjs/jwt";
import { jwtConstants } from './constants';
import { JwtStrategy } from "src/auth/jwt.strategy";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";

@Module({
    imports: [
        UserModule,
        PassportModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: {expiresIn: jwtConstants.expiresIn},
        }),
    ],

    providers: [
        AuthService,
        LocalStrategy,
        JwtStrategy,
        JwtAuthGuard,
    ],

    exports: [AuthService],
})
export class AuthModule {
}
