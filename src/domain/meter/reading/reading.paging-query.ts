import { ApiProperty } from "@nestjs/swagger";
import { IsIn } from "class-validator";
import { BasePagingQuery } from "src/domain/common/base.paging-query";
import { Reading } from "src/domain/meter/reading/reading.entity";
import { OrderableKeys } from "src/domain/common/pagination.types";

const readingKeys: string[] = Object.keys(getFilledReading())

export class ReadingPagingQuery extends BasePagingQuery {
    @ApiProperty()
    @IsIn(readingKeys)
    field: OrderableKeys<Reading> = 'timestamp'
}

function getFilledReading(): Reading {
    const reading = new Reading()

    reading.id = ""
    reading.timestamp = new Date()
    reading.counter = 0

    return reading
}
