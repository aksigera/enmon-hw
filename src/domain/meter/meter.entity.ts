import { Cascade, Collection, Entity, OneToMany } from "@mikro-orm/core";
import { Reading } from "src/domain/meter/reading/reading.entity";
import { BaseEntity } from "src/domain/common/base.entity";

@Entity()
export class Meter extends BaseEntity {
    @OneToMany(() => Reading, r => r.meter, {cascade: [Cascade.ALL]})
    readings = new Collection<Reading>(this);
}
