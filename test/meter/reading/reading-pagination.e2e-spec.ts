import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { MeterProvider } from "test/meter/meter.provider";
import * as _ from "lodash";
import { ReadingDto } from "src/domain/meter/reading/reading.dto";
import { testModule } from "test/util/init.util";

/** These should be merged into single suite.
 * The reason it's not is that currently in-memory DB is preserved between tests. */
describe('pagination', () => {
    let app: INestApplication;

    it('Pages correctly', async () => {
        const timeStump = "2010-10-0"
        const readings: Awaited<ReadingDto>[] = await Promise.all(_.range(1, 10).map((num) =>
            MeterProvider.postReading(app, {timestamp: timeStump + num, counter: 100 - num})))

        const resDefault = await request(app.getHttpServer())
            .get('/meter/reading');

        expect(resDefault.body[0]).toEqual(readings)

        const limit = 3
        const offset = 2
        const field = 'counter'

        const resCustom = await request(app.getHttpServer())
            .get(`/meter/reading?limit=${limit}&offset=${offset}&field=${field}`);

        expect(resCustom.body[0]).toEqual(_(readings).sortBy([field]).drop(offset).take(limit).value())
    });

    beforeEach(async () => {
        app = (await testModule()).createNestApplication();

        app.useGlobalPipes(new ValidationPipe({transform: true}));
        await app.init();
    });
})
